library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity seven_segment_decoder is
	port (
		sw: in std_ulogic_vector(3 downto 0);
		blank: in std_ulogic;
		hex0: out std_ulogic_vector(6 downto 0)
	);
end entity;

architecture combinatorial of seven_segment_decoder is
	signal hex: std_ulogic_vector(6 downto 0);
begin
	with sw select hex <=
		not "0111111" when "0000", -- 0
		not "0000110" when "0001", -- 1
		not "1011011" when "0010", -- 2
		not "1001111" when "0011", -- 3
		not "1100110" when "0100", -- 4
		not "1101101" when "0101", -- 5
		not "1111101" when "0110", -- 6
		not "0000111" when "0111", -- 7
		not "1111111" when "1000", -- 8
		not "1101111" when "1001", -- 9
		not "1110111" when "1010", -- A
		not "1111100" when "1011", -- b
		not "0111001" when "1100", -- C
		not "1011110" when "1101", -- d
		not "1111001" when "1110", -- E
		not "1110001" when "1111", -- F
		not "0000000" when others;
	with blank select hex0 <=
		hex when '0',
		not "0000000" when others;
end architecture;

		
