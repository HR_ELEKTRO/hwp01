library ieee;
use ieee.std_logic_1164.all;

entity my_first_component is
    port (
        inputs: in std_ulogic_vector (3 downto 0);
        outputs: out std_ulogic_vector (6 downto 0)
    );
end my_first_component;

architecture implementation of my_first_component is
begin
    outputs(0) <= inputs(0); 
    outputs(1) <= inputs(1);
    outputs(2) <= inputs(0) or inputs(1);
    outputs(3) <= inputs(0) and inputs(1);
    outputs(4) <= inputs(0) and inputs(1) and inputs(2) and inputs(3); 
    outputs(5) <= inputs(0) or  inputs(1) or inputs(2) or inputs(3);
    outputs(6) <= not(inputs(0) or inputs(1) or inputs(2) or inputs(3));
end implementation;